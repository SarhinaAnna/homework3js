let number = +prompt("Enter your number");

let res = "Sorry, no numbers";
let flag = true;

if (number < 5) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 0; i <= number; i++) {
    if (i % 5 === 0) {
      console.log(i);
      flag = false;
    }
  }

  if (flag) {
    console.log(res);
  }
}
